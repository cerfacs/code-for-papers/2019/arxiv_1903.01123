# HOW TO

The data and saved weight of the models can be downloaded here : "https://cerfacs.fr/opendata/documents/arxiv_1903.01123/data.tgz"

If needed, you can also download trained weight for the models here : "https://cerfacs.fr/opendata/documents/arxiv_1903.01123/save_model.tgz"